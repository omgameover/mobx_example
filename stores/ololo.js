import { observable, action } from 'mobx'

class OloloStore {
  @observable list = []

  @action
  changeElement(id, value) {
    this.list[id] = value
  }
}

export default new OloloStore()
