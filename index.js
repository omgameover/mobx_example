import React from 'react'
import ReactDOM from 'react-dom'
import createBrowserHistory from 'history/createBrowserHistory'
import { Provider } from 'mobx-react'
import { RouterStore, syncHistoryWithStore } from 'mobx-react-router'
import { Router, Route, Switch, Redirect } from 'react-router'
import App from './App'
import registerServiceWorker from './registerServiceWorker'
import stores from './stores'

const browserHistory = createBrowserHistory()
const routingStore = new RouterStore()
const history = syncHistoryWithStore(browserHistory, routingStore)
const storesWithRouting = Object.assign(stores, { routing: routingStore })

ReactDOM.render(
  <Provider {...storesWithRouting}>
    <div>
      <Router history={history}>
        <Route
          path="/"
          render={ () => <App /> }
        />
      </Router>
    </div>
  </Provider>,
  document.getElementById('root')
)
registerServiceWorker()
